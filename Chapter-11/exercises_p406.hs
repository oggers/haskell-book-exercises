-- |

module exercises_p406 where

-- 1. Given the type

data FlowerType = Gardenia
                | Daisy
                | Rose
                | Lilac
                deriving Show

type Gardener = String

data Garden =
  Garden Gardener FlowerType
  deriving Show

-- What is the normal form of Garden?

data GardenNormal =
  Gardenia Gardener
  | Daisy Gardener
  | Rose Gardener
  | Lolac Gardener
  deriving Show
