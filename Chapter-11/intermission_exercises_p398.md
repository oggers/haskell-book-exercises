# Intermission: Exercises

1. Given a datatype

data BigSmall =
    Big Bool
    | Small Bool
    deriving (Eq, Show)


what is the cardinality of this datatype? 4

2. Given a datatype
-- needed to have Int8 in scope
import Data.Int

data NumberOrBool =
  Numba Int8
  | BoolyBool Bool
  deriving (Eq, Show)
  
-- Example use of Numba, parenthesis due to
-- syntactic collision between (-) minus and
-- the negate function
let myNumba = Numba (-128)

What is the cardinality of NumberOrBool? 256 + 2 = 258
What happens if you try to create a Numba with a numeric
larger than 127? 
