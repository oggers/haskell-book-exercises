-- |

module Jammin where
import Data.List

data Fruit =
  Peach
  | Plum
  | Apple
  | Blackberry
  deriving (Eq, Show, Ord)

data JamJars =
  Jam Fruit Int
  deriving (Eq, Show, Ord)


-- 2. Rewrite JamJars with record syntax

data JamJarsR =
  JamR { fruit :: Fruit
       , quantity :: Int }
  deriving (Eq, Show, Ord)

-- 3. What is the cardinality of JamJars? 4 * cardinality of Int

-- 4. Add Ord instances to your deriving clauses.

-- 5. You can use the record field accessors in other functions as well.
--    To demonstrate this, work up some sample data that has a count
--    of the types and numbers of jars of jam in the rows in our pantry
--    (you can define more data than this if you like):

row1 = JamR Peach 10
row2 = JamR Peach 4
row3 = JamR Plum 3
row4 = JamR Apple 6
row5 = JamR Blackberry 20
row6 = JamR Blackberry 5
allJam = [row1, row2, row3, row4, row5, row6]

-- Now over that list of data, we can map the field accessor for the
-- Int value and see a list of the numbers for each row.

listOfNumbers = map quantity allJam

-- 6. Write a function that will return the total number of jars of jam.

totalJars :: [JamJarsR] -> Int
totalJars x = sum $ map quantity x

-- 7. Write a function that will tell you which row has the most jars of
--    jam in it. It should return a result like this, though the fruit and
--    number will vary depending on how you defined your data:

mostRow :: [JamJarsR] -> JamJarsR
mostRow = maximumBy (\x y -> compare (quantity y) (quantity x))

-- 8. Under your module name, import the module called Data.List.
--    It includes some standard functions called sortBy and groupBy
--    that will allow us to organize our list of jams. Look at their type
--    signatures because there are some important differences between
--    them.

-- 9. You'll want to sort the list allJams by the first field in each record.
--    You may (or may not) want to use the following helper function
--    as part of that:
compareKind (JamR k _) (JamR k' _) = compare k k'

sortedAllJams = sortBy compareKind allJam

-- 10. Now take the sorting function and use groupBy to group the
--     jams by the type of fruit they are made from. You'll later want
--     the ability to sum the sublists separately, so you're looking for a
--     result that is a list of lists (again, the actual data in your list will
--     depend on how you defined it):

groupJam = groupBy (\k k' -> fruit k == fruit k') allJam
