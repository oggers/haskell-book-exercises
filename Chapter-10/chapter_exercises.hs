-- |

module nil where

myAnd :: [Bool] -> Bool
myAnd = foldr (&&) True

myOr :: [Bool] -> Bool
myOr = foldr (||) False

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr (\x y -> f x || y) False

myElem :: Eq a => a -> [a] -> Bool
myElem x = any (\a -> a == x)

myReverse :: [a] -> [a]
--myReverse = foldr (\x y ->  y ++ [x]) []
myReverse = foldl (flip (:)) []

myMap :: (a -> b) -> [a] -> [b]
--myMap f = foldr (\x y -> f x : y) []
myMap f = foldr ((:) . f) []

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f = foldr (\x y -> if f x then x:y else y) []

squish :: [[a]] -> [a]
--squish = foldr (\x y -> concat [x, y]) []
squish = foldr (++) []

squishMap :: (a -> [b]) -> [a] -> [b]
--squishmap f = foldr (\x y -> concat [f x, y]) []
squishMap f = foldr ((++) . f) []

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
{--myMaximumBy f (x:xs) =
  foldr (\curr acc -> if f curr acc == GT then curr else acc)
        x
        xs
-}
myMaximumBy f (x : xs) = foldr g x xs where
    g x x'
      | ((f x x') == GT) = x
      | otherwise = x'

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy f (x:xs) = head $
  foldr (\a b -> if f (head b) a == LT then concat [b, [a]] else a:b)
        [x]
        xs
